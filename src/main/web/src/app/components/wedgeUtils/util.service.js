(function(){
	'use strict';

	angular
		.module('wedgesvg')
		.factory('WedgeUtils', wedgeUtils);

	/** @ngInject */
	function wedgeUtils($document, $location) {
		return {
			isSVGSupported: isSVGSupported,
      getDomainUrl: getDomainUrl
		};

		function isSVGSupported() {
			return $document[0].implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1");
		}

    function getDomainUrl() {
      var host = $location.host();
      if (host.startsWith('localhost')) {
        return '';
      }
      else {
        var protocol = $location.protocol();
        var port = $location.port();
        var domainUrl = protocol + '://' + host;
        if (port !== 80 && port !== 443) {
          domainUrl += ':' + port;
        }

        var fullPath = $location.absUrl();
        var contextPath = fullPath.replace(domainUrl, '');
        contextPath = contextPath.substring(0, contextPath.indexOf('#') - 1);
        domainUrl += contextPath;
        return domainUrl;
      }
    }
	}
})();
