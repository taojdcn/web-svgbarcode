(function(){
	'use restrict';

	angular
		.module('wedgesvg')
		.directive('wedgeBarcard', barcard);

	/** @ngInject */
	function barcard() {
		var directive = {
			restrict: 'E',
			templateUrl: 'app/components/barcard/barcard.html',
			scope: {
				barcode: '=barcode'
			},
			controller: barcardController,
			controllerAs: 'bc',
			bindToController: true
		};

		return directive;

		/** @ngInject */
		function barcardController($location) {
			var vm = this;

			vm.navTo = function(path) {
				$location.path('/code/' + path);
			}
		}
	}
})();