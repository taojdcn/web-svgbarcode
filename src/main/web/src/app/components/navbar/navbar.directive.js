(function() {
  'use strict';

  angular
    .module('wedgesvg')
    .directive('wedgeNavbar', wedgeNavbar);

  /** @ngInject */
  function wedgeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          activeItem: '@'
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController() {
      var vm = this;
      vm.activeItem = null;
    }
  }

})();
