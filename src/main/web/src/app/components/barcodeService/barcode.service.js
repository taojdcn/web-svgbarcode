(function() {
	'use strict';

	angular
		.module('wedgesvg')
		.factory('BarcodeService', barcodeService);

	/** @ngInject */
	function barcodeService($http, $sce, WedgeUtils, RestEntries) {
		var barcodeUrl = WedgeUtils.getDomainUrl() + RestEntries.barcodeUrl;

		return {
			generate: function(content, type, settings) {
				var req = {
					method: 'POST',
					url: barcodeUrl,
					headers: {
						'Content-Type': 'application/json'
					},
					data: {
						content: content,
						type: type,
						settings: settings
					},
          transformResponse: function(data, headerGetter, status) {
            if (status === 200) {
              return $sce.trustAsHtml(data);
            } else {
              return data;
            }
          }
				};
				return $http(req);
			}
		}
	}

})();
