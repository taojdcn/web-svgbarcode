(function() {
  'use strict';

  angular
    .module('wedgesvg')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
