(function() {
  'use strict';

  angular
    .module('wedgesvg')
    .controller('SubscrController', subscrController);

  /** @ngInject */
  function subscrController() {
    var vm = this;
    vm.userEmail = '';

    vm.isEmailValid = function() {
      return true;
    };

    init();

    function init() {
      // do nothing
    }
  }
})();
