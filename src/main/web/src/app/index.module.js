(function() {
  'use strict';

  /* app main module */
  angular
    .module('wedgesvg', ['ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ngRoute', 'mgcrea.ngStrap', 'toastr', 'base64', 'ngclipboard']);
})();