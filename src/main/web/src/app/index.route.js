(function() {
  'use strict';

  angular
    .module('wedgesvg')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/code/:format', {
        templateUrl: 'app/generator/code.html',
        controller: 'GeneratorController',
        controllerAs: 'gc'
      })
      .when('/api', {
        templateUrl: 'app/api/api.html'
      })
      .when('/subscription', {
        templateUrl: 'app/subscription/subscription.html',
        controller: 'SubscrController',
        controllerAs: "sc"
      })
      .when('/orderconfirm', {
        templateUrl: 'app/subscription/confirmation.html'
      })
      .when('/oops', {
        templateUrl: 'app/errorfacet/error.html',
        controller: 'GlobalErrorController',
        controllerAs: 'gec'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
