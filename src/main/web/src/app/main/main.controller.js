(function() {
  'use strict';

  angular
    .module('wedgesvg')
    .controller('MainController', mainController);

  /** @ngInject */
  function mainController($timeout, barcodes) {
    var vm = this;

    vm.barcodes = barcodes;
    vm.activeItem = 'home';

    init();

    function init() {
      // do nothing
    }
  }
})();
