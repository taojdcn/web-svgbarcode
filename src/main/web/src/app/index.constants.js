/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('wedgesvg')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    /* eslint-disable angular/window-service*/
    .constant('_', window._)
    .constant('RestEntries', {
      barcodeUrl: '/api/v1/barcode'
    })
    .constant('barcodes', [
							{
								name: "QR Code",
								format: "QR_CODE",
								category: "2D",
								properties: [
									{
										name: "ERROR_CORRECTION",
										description: "Error Correction Level",
										type: "Select",
										options: [
											{
												label: "Low",
												value: "L"
											},
											{
												label: "Medium",
												value: "M"
											},
											{
												label: "Hight",
												value: "H"
											}
										],
										default: "L"
									},
									{
										name: "MARGIN",
										description: "Border Margin",
										type: "Number",
										default: 2
									}
								]
							},
							{
								name: "AZTEC Code",
								format: "AZTEC",
								category: "2D",
								properties: [
									{
										name: "CHARACTER_SET",
										description: "Character Set",
										type: "Select",
										options: [
											{
												label: "ISO-8859-1",
												value: "ISO-8859-1"
											},
											{
												label: "US-ASCII",
												value: "US-ASCII"
											},
											{
												label: "UTF-8",
												value: "UTF-8"
											},
											{
												label: "UTF-16",
												value: "UTF-16"
											}
										],
										default: "ISO-8859-1"
									},
									{
										name: "ERROR_CORRECTION",
										description: "Error Correction Level",
										type: "Range",
										minimum: 0,
										maximum: 100,
										default: 33
									},
									{
										name: "AZTEC_LAYERS",
										description: "Layers",
										type: "Range",
										minimum: -4,
										maximum: 32,
										default: 0
									},
									{
										name: "MARGIN",
										description: "Border Margin",
										type: "Number",
										default: 2
									}
								]
							},
							{
								name: "Data Matrix",
								format: "DATA_MATRIX",
								category: "2D",
								properties: [
									{
										name: "DATA_MATRIX_SHAPE",
										description: "Data Matrix Shape",
										type: "Select",
										options: [
											{
												label: "None",
												value: "FORCE_NONE"
											},
											{
												label: "Square",
												value: "FORCE_SQUARE"
											},
											{
												label: "Rectangle",
												value: "FORCE_RECTANGLE"
											}
										],
										default: "FORCE_NONE"
									},
									{
										name: "MARGIN",
										description: "Border Margin",
										type: "Number",
										default: 2
									}
								]
							},
                          {
                            name: "PDF 417",
                            format: "PDF_417",
                            category: "2D",
                            properties: [
                              {
                                name: "PDF417_COMPACT",
                                description: "Compact Format",
                                type: "Select",
                                options: [
                                  {
                                    label: "Yes",
                                    value: "true"
                                  },
                                  {
                                    label: "No",
                                    value: "false"
                                  }
                                ],
                                default: "false"
                              },
                              {
                                name: "PDF417_COMPACTION",
                                description: "Encoding Schemes",
                                type: "Select",
                                options: [
                                  {
                                    label: "AUTO",
                                    value: "AUTO"
                                  },
                                  {
                                    label: "TEXT",
                                    value: "TEXT"
                                  },
                                  {
                                    label: "BYTE",
                                    value: "BYTE"
                                  },
                                  {
                                    label: "NUMERIC",
                                    value: "NUMERIC"
                                  }
                                ],
                                default: "AUTO"
                              },
                              {
                                name: "CHARACTER_SET",
                                description: "Character Set",
                                type: "Select",
                                options: [
                                  {
                                    label: "ISO-8859-1",
                                    value: "ISO-8859-1"
                                  },
                                  {
                                    label: "US-ASCII",
                                    value: "US-ASCII"
                                  },
                                  {
                                    label: "UTF-8",
                                    value: "UTF-8"
                                  },
                                  {
                                    label: "UTF-16",
                                    value: "UTF-16"
                                  }
                                ],
                                default: "ISO-8859-1"
                              },
                              {
                                name: "ERROR_CORRECTION",
                                description: "Error Correction Characters",
                                type: "Range",
                                minimum: 2,
                                maximum: 512,
                                default: 2
                              },
                              {
                                name: "MARGIN",
                                description: "Border Margin",
                                type: "Number",
                                default: 2
                              }
                            ]
                          },
							{
								name: "Code 128",
								format: "CODE_128",
								category: "1D",
								properties: [
									{
										name: "MH",
										description: "Side Margin",
										type: "Number",
										default: 10
									},
									{
										name: "MV",
										description: "Head Footer Margin",
										type: "Number",
										default: 0
									},
									{
										name: "R",
										description: "Height/Width Ratio",
										type: "Number",
										default: 0.2
									}
								]
							}
						]);

})();
