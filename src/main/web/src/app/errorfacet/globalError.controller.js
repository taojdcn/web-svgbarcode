(function() {
	'use strict';

	angular
		.module('wedgesvg')
		.controller('GlobalErrorController', globalErrorController);

	function globalErrorController(GlobalErrorService) {
		var vm = this;
		vm.errorMsg = null;
		vm.init = init;

		init();

		function init() {
			vm.errorMsg = GlobalErrorService.getErrMsg();
		}
	}
})();