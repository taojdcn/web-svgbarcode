(function(){
	'use strict';

	angular
		.module('wedgesvg')
		.factory('GlobalErrorService', globalErrorService);

	/** @ngInject */
	function globalErrorService(_) {
		var defaultError = 'URL does not found!';
		var errorMsg = null;

		return {
			getErrMsg: function() {
				return _.isEmpty(errorMsg) ? defaultError : errorMsg;
			},
			setErrMsg: function(err) {
				errorMsg = err;
			}
		}
	}
})();