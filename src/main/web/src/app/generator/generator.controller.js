(function() {
	'use strict';

	angular
		.module('wedgesvg')
		.controller('GeneratorController', generatorController);

	/** @ngInject */
	function generatorController($routeParams, $log, $base64, $location, _, BarcodeService, GlobalErrorService, WedgeUtils, barcodes) {
		var vm = this;
		vm.barcode = null;
		vm.settings = [];
		vm.barcodeContent = null;
		vm.processing = false;
		vm.xmlData = null;
		vm.base64Data = null;
		vm.output = null;
		vm.bgStyle = null;
		vm.isXmlFormat = true;
		vm.errorMsg = null;

		vm.generateBarcode = generateBarcode;
		vm.updateResultFormat = updateResultFormat;
		vm.displayResult = displayResult;
		vm.dismissAlert = dismissAlert;
    vm.previewAvailable = WedgeUtils.isSVGSupported();

		init();

		function init() {
			vm.barcode = _.find(barcodes, function(el){
				return el.format === $routeParams.format;
			});

			// TODO: navigate to 404 if no code found
			if(_.isUndefined(vm.barcode)) {
				$log.error('No generator found for ' + $routeParams.format);
				GlobalErrorService.setErrMsg('No code generator found for ' + $routeParams.format);

				$location.path('/oops');
			} else {
				_.each(vm.barcode.properties, function(el){
					vm.settings.push(
						{
							definition: el,
							value: el.default
						}
					);
				});
			}
		}

		function generateBarcode() {
			vm.processing = true;
			vm.xmlData = null;
			vm.base64Data = null;
			vm.bgStyle = null;
			vm.output = null;
			vm.errorMsg = null;
			if (_.isEmpty(vm.barcodeContent)) {
				vm.errorMsg = "You have to fill some content to be converted into bar code.";
				vm.processing = false;
				return;
			}
			var userSettings = extractSettingValues();
			BarcodeService.generate(vm.barcodeContent, vm.barcode.format, userSettings).then(
				function(response){
					vm.xmlData = response.data;
					vm.output = vm.xmlData;
					vm.base64Data = $base64.encode(vm.xmlData);
					vm.bgStyle = {
						'background-image': 'url("data:image/svg+xml;base64,' + vm.base64Data + '")'
					};
					vm.processing = false;
				},
				function(response){
					if (response.status === 404) {
						vm.errorMsg = 'Web service is not available';
					}
					else {
            if(_.isEmpty(response.data)) {
              vm.errorMsg = 'Unknown error! Error Code: ' + response.status + '. Please contact administrator or try it later';
            }
            else {
              vm.errorMsg = response.data;
            }
					}
					vm.processing = false;
				}
			);
		}

		function displayResult() {
			var showResult = !(_.isEmpty(vm.output));
			// $log.info('To show result: ' + showResult);
			return showResult;
		}

		function updateResultFormat(format) {
			if (vm.isXmlFormat && format === 'base64') {
				vm.isXmlFormat = false;
				vm.output = vm.base64Data;
			}
			else if (!vm.isXmlFormat && format === 'xml') {
				vm.isXmlFormat = true;
				vm.output = vm.xmlData;
			}
		}

		function extractSettingValues() {
			var userSettings = vm.settings.map(function(el){
				return {name: el.definition.name, value: el.value};
			});

			return userSettings;
		}

		function dismissAlert() {
			vm.errorMsg = null;
		}
	}
})();
