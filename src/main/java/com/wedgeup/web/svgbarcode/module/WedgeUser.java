package com.wedgeup.web.svgbarcode.module;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by taodong on 11/11/16.
 */

@NamedQueries({
        @NamedQuery(name="WedgeUser.getUserByPaymentId", query="SELECT e FROM WedgeUser e WHERE e.paymentId = :paymentId")
})
@Entity
@Table(name="usr_user")
public class WedgeUser {
    @Id
    private String id;
    private String status = "ACTIVE";
    private String accessCode;
    private int apiLimit;
    private String paymentId;
    private Date expirationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public int getApiLimit() {
        return apiLimit;
    }

    public void setApiLimit(int apiLimit) {
        this.apiLimit = apiLimit;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
