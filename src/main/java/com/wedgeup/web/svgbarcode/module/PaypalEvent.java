package com.wedgeup.web.svgbarcode.module;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

/**
 * Created by taodong on 11/9/16.
 */
public class PaypalEvent implements Serializable{
    private String tnxId;
    private String type;
    private String userEmail;
    private String payerEmail;
    private String payerId;
    // customer's choice on subscription: 0 for default, 1 for 2000 api monthly api limits
    private int action;

    public String getTnxId() {
        return tnxId;
    }

    public void setTnxId(String tnxId) {
        this.tnxId = tnxId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tnxId", tnxId)
                .add("type", type)
                .add("userEmail", userEmail)
                .add("payerEmail", payerEmail)
                .add("payerId", payerId)
                .add("action", action)
                .toString();
    }
}
