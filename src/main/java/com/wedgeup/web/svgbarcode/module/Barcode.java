package com.wedgeup.web.svgbarcode.module;

import com.google.common.base.MoreObjects;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by taodong on 11/11/16.
 */
public class Barcode {
    @NotNull
    private String content;
    private String format;
    @NotNull
    private String type;

    private List<BarcodeSetting> settings;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public List<BarcodeSetting> getSettings() {
        return settings;
    }

    public void setSettings(List<BarcodeSetting> settings) {
        this.settings = settings;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("content", content)
                .add("format", format)
                .add("type", type)
                .add("settings", settings)
                .toString();
    }
}

