package com.wedgeup.web.svgbarcode.module;

import com.google.common.base.MoreObjects;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by taodong on 8/23/16.
 */

public class EmailRecord implements Serializable{
    private String id;
    private String subject;
    private String content;
    private String fromAddress;
    private String toAddress;
    private String ccAddress;
    private String bccAddress;
    private String tags;
    private String status;
    private String transactionId;
    private Date captureDate;
    private Date updateDate;
    private String domain;
    private String ignoreConsent;
    private boolean testMode = false;
    private boolean inHTMLFormat = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getCcAddress() {
        return ccAddress;
    }

    public void setCcAddress(String ccAddress) {
        this.ccAddress = ccAddress;
    }

    public String getBccAddress() {
        return bccAddress;
    }

    public void setBccAddress(String bccAddress) {
        this.bccAddress = bccAddress;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Date getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(Date captureDate) {
        this.captureDate = captureDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getIgnoreConsent() {
        return ignoreConsent;
    }

    public void setIgnoreConsent(String ignoreConsent) {
        this.ignoreConsent = ignoreConsent;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isInHTMLFormat() {
        return inHTMLFormat;
    }

    public void setInHTMLFormat(boolean inHTMLFormat) {
        this.inHTMLFormat = inHTMLFormat;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).omitNullValues()
                .add("fromAddress", fromAddress)
                .add("toAddress", toAddress)
                .add("domain", domain)
                .add("transactionId", transactionId)
                .add("testMode", testMode)
                .toString();
    }
}
