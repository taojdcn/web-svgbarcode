package com.wedgeup.web.svgbarcode.exception;

/**
 * Created by tao on 3/27/16.
 */
public class BadRequestException extends RuntimeException {
    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
