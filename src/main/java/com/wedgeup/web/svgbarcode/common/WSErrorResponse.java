package com.wedgeup.web.svgbarcode.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by taodong on 8/26/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WSErrorResponse {
    private final String status = "FAILED";
    private final String message;
    private final List<String> metadata;

    public WSErrorResponse(String message, List<String> metadata) {
        this.message = message;
        this.metadata = metadata;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getMetadata() {
        return metadata;
    }
}
