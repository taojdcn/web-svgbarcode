package com.wedgeup.web.svgbarcode.common;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

/**
 * Created by taodong on 9/22/16.
 */
@Component
public class HttpClientFactory {
    private final int MAX_CONNECTIONS_DEFAULT = 50;
    private final int MAX_PER_ROUTE_CONNECTIONS_DEFAULT = 20;
    private final int SOCKET_TIMEOUT_DEFAULT = 120000;
    private final int CONNECTION_TIMEOUT_DEFAULT = 60000;
    private final int VALIDATE_AFTER_INACTIVITY_DEFAULT = 5000;

    private final SocketConfig socketConfig = SocketConfig.custom().setTcpNoDelay(true).build();
    private final SSLContext sslContext = SSLContexts.createDefault();
    private final SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,(HostnameVerifier) null);
    private final ConnectionSocketFactory connectionSocketFactory = new PlainConnectionSocketFactory();
    private final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create().register("https", sslConnectionSocketFactory).register("http", connectionSocketFactory).build();
    private final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
    private final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(CONNECTION_TIMEOUT_DEFAULT).setSocketTimeout(SOCKET_TIMEOUT_DEFAULT).build();

    @PostConstruct
    public void init() {
        connectionManager.setDefaultMaxPerRoute(MAX_PER_ROUTE_CONNECTIONS_DEFAULT);
        connectionManager.setMaxTotal(MAX_CONNECTIONS_DEFAULT);
        connectionManager.setValidateAfterInactivity(VALIDATE_AFTER_INACTIVITY_DEFAULT);
    }

    public CloseableHttpClient createDefaultHttpClient() {
        CloseableHttpClient client = HttpClientBuilder.create()
                .setDefaultSocketConfig(socketConfig).setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager).build();

        return client;
    }

}
