package com.wedgeup.web.svgbarcode.common;

import com.wedgeup.utils.lang.ToStringHelper;
import com.wedgeup.web.svgbarcode.exception.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by taodong on 8/25/16.
 */
@ControllerAdvice
public class RestErrorHandler {
    private static Logger log = LoggerFactory.getLogger(RestErrorHandler.class);

    @ExceptionHandler({BadRequestException.class, MethodArgumentNotValidException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public WSErrorResponse requestValidationFailed(MethodArgumentNotValidException e) {

        WSErrorResponse entity = new WSErrorResponse("Bad Request. Please fix your request data.", null);

        return entity;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public WSErrorResponse requestValidationFailed(Exception e, HttpServletRequest req) {
        log.error("FIX-IT: Unhandled exception occurs while process {}. 500 error is returned to customer.", req.getRequestURL(), e);
        return new WSErrorResponse(errMsg500, null);
    }

    @Value("${msg.err.500}")
    private String errMsg500;
}

