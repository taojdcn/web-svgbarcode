package com.wedgeup.web.svgbarcode.configuration;

import com.wedgeup.utils.lang.ToStringHelper;
import com.wedgeup.web.svgbarcode.dao.UserDao;
import com.wedgeup.web.svgbarcode.module.WedgeUser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Date;

/**
 * Created by taodong on 11/11/16.
 */
@EnableWebSecurity
@Configuration
public class WebAuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
    private static Logger log = LoggerFactory.getLogger(WebAuthenticationConfiguration.class);

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        super.init(auth);
    }

    @Bean
    UserDetailsService userDetailsService() {
        return (String userName) ->  {
            log.info("Authenticating user {}", userName);
            WedgeUser wedgeUser = userDao.readUserById(userName);
            if (wedgeUser != null) {
                String password = wedgeUser.getAccessCode();
                boolean enabled = false;
                boolean nonLocked = false;
                String status = wedgeUser.getStatus();
                if(StringUtils.equalsIgnoreCase("ACTIVE", status)) {
                    enabled = true;
                    nonLocked = true;
                } else if (StringUtils.equalsIgnoreCase("LOCKED", status)) {
                    enabled = true;
                }
                Date expiration = wedgeUser.getExpirationDate();
                boolean nonExpired = expiration.after(new Date());
                return new User(userName, password, enabled, nonExpired, true, nonLocked, AuthorityUtils.createAuthorityList("USER"));
            } else {
                throw new UsernameNotFoundException(ToStringHelper.join(null, "could not find the wedgeUser", userName));
            }
        };
    }

    @Autowired
    private UserDao userDao;

}
