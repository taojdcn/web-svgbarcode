package com.wedgeup.web.svgbarcode.dao;

import com.wedgeup.web.svgbarcode.module.APIUsage;
import com.wedgeup.web.svgbarcode.module.WedgeUser;

/**
 * Created by taodong on 11/11/16.
 */
public interface UserDao {
    /**
     * retrieve user detail by id
     * @param id - user id
     * @return
     */
    WedgeUser readUserById(String id);

    /**
     * retrieve user by payment id
     * @param paymentId
     * @return
     */
    WedgeUser readUserByPaymentId(String paymentId);

    /**
     * create a wedgeUser record
     * @param wedgeUser
     * @return wedgeUser record
     */
    void createUser(WedgeUser wedgeUser);

    /**
     * updatea wedgeUser record
     * @param wedgeUser
     * @return
     */
    void updateUser(WedgeUser wedgeUser);

    /**
     * create a user api usage record
     * @param usage
     */
    void createAPIUsage(APIUsage usage);

    /**
     * find api usage of a wedgeUser
     * @param wedgeUser
     * @return
     */
    int readUsageForUser(WedgeUser wedgeUser);
}
