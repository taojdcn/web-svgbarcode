package com.wedgeup.web.svgbarcode.dao.impl;

import com.wedgeup.web.svgbarcode.dao.UserDao;
import com.wedgeup.web.svgbarcode.module.APIUsage;
import com.wedgeup.web.svgbarcode.module.WedgeUser;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * Created by taodong on 11/11/16.
 */

@Repository
public class UserDaoImpl implements UserDao {
    @Override
    @Cacheable("wedge-users")
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public WedgeUser readUserById(String id) {
        return em.find(WedgeUser.class, id);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public WedgeUser readUserByPaymentId(String paymentId) {
        WedgeUser user = null;
        List<WedgeUser> users = em.createNamedQuery("WedgeUser.getUserByPaymentId")
                .setParameter("paymentId", paymentId).setMaxResults(1).getResultList();
        if (users != null && !users.isEmpty()) {
            user = users.get(0);
        }
        return user;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public void createUser(WedgeUser wedgeUser) {
        em.persist(wedgeUser);
    }

    @Override
    @CacheEvict(value="wedge-users", key="#wedgeUser.id")
    @Transactional(propagation = Propagation.SUPPORTS)
    public void updateUser(WedgeUser wedgeUser) {
        em.merge(wedgeUser);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public void createAPIUsage(APIUsage usage) {
        em.persist(usage);
    }

    @Override
    public int readUsageForUser(WedgeUser wedgeUser) {
        Date endDate = wedgeUser.getExpirationDate();
        DateTime jExp = new DateTime(endDate);
        DateTime jStart = jExp.minusDays(40);
        Date startDate = jStart.toDate();
        int usage = (Integer)em.createNamedQuery("APIUsage.getUsage").setParameter("startDate", startDate)
                .setParameter("endDate", endDate).getSingleResult();
        return usage;
    }

    @PersistenceContext
    private EntityManager em;
}
