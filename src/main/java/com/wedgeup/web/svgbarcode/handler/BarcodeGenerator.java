package com.wedgeup.web.svgbarcode.handler;

import com.google.common.base.Joiner;
import com.wedgeup.barsvg.api.BarcodeSVGGenerator;
import com.wedgeup.web.svgbarcode.module.Barcode;
import com.wedgeup.web.svgbarcode.module.BarcodeSetting;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by taodong on 11/11/16.
 */
@Component
public class BarcodeGenerator {
    public String generateBarcode(Barcode barcode) throws Exception {
        Map<String, String> configs = new HashMap<>();
        String content = barcode.getContent();
        String type = barcode.getType();

        if (StringUtils.isBlank(content)) {
            throw new IllegalArgumentException("Content is required.");
        }

        if (barcode.getSettings() != null) {
            String marginSetting = null;
            for (BarcodeSetting setting : barcode.getSettings()) {
                boolean skipSetting = false;
                switch (setting.getName()) {
                    case "MH":
                    case "MV":
                    case "R":
                        skipSetting = true;
                        marginSetting = Joiner.on(";;").skipNulls().join(marginSetting, Joiner.on(":").useForNull("").join(setting.getName(), setting.getValue()));
                        break;
                }
                if (!skipSetting) {
                    configs.put(setting.getName(), setting.getValue());
                }
            }

            if (StringUtils.isNotBlank(marginSetting)) {
                configs.put("MARGIN", marginSetting);
            }
        }

        String code = BarcodeSVGGenerator.generate(type, content, configs, "Created by www.barcodesvg.io");
        return code;
    }
}
