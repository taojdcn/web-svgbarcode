package com.wedgeup.web.svgbarcode.handler;

import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by tao on 11/21/16.
 */
@Component
public class FileResourceLoader {

    private static Logger log = LoggerFactory.getLogger(FileResourceLoader.class);

    /**
     * load text file from class path
     * @param fileName - file name with relative path to classpath root
     * @return string of file content or null if error
     */
    @Cacheable("templates")
    public String loadTextFileFromClasspath(String fileName) {
        ClassPathResource resource = new ClassPathResource(fileName);
        String content = null;
        if (resource.exists()) {
            try (InputStream is = resource.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                String line;
                while((line = reader.readLine()) != null) {
                    content = Joiner.on("\n").skipNulls().join(content, line);
                }
            } catch (IOException e) {
                log.error("Failed to load file {}", fileName, e);
            }
        } else {
            log.error("Failed to load file. File {} doesn't exist in classpath", fileName);
        }
        return content;
    }
}
