package com.wedgeup.web.svgbarcode.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.wedgeup.utils.lang.StringGenerator;
import com.wedgeup.web.svgbarcode.common.HttpClientFactory;
import com.wedgeup.web.svgbarcode.common.WSErrorResponse;
import com.wedgeup.web.svgbarcode.dao.UserDao;
import com.wedgeup.web.svgbarcode.module.EmailRecord;
import com.wedgeup.web.svgbarcode.module.PaypalEvent;
import com.wedgeup.web.svgbarcode.module.WedgeUser;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * Created by taodong on 11/13/16.
 */
@Component
public class PaypalMessageHandler implements MessageListener {
    private static Logger log = LoggerFactory.getLogger(PaypalMessageHandler.class);

    private static final String DEFAULT_TEMPLATE = Joiner.on("\n").skipNulls().join(
            "<!DOCTYPE html>",
            "<html>",
                "<body>",
                    "<p>Dear customer</p>",
                    "<p>Your order of API usage on www.svgbarcode.com has been confirmed. The following is your order detail:</p>",
                    "<p><b>Username</b>:&nbsp;&nbsp;&nbsp;&nbsp;{{userEmail}}</p>",
                    "<p><b>Password:</b>&nbsp;&nbsp;&nbsp;&nbsp;{{accessCode}}</p>",
                    "<p><b>Monthly API limits:</b>&nbsp;&nbsp;&nbsp;&nbsp;{{apiLimit}}</p>",
                    "<p>If you have any question or need any help, please email support@wedgeup.com. Thank you for your business</p>",
                    "<p>Sincerely</p>",
                    "<p>Wedgeup Support Team</p>",
                "</body>",
            "</html>"
    );

    @JmsListener(destination = "${queue.paypal.activity}", containerFactory = "jmsListenerFactory")
    @Transactional
    @Override
    public void onMessage(Message message) {
        try {
            String payload = ((TextMessage)message).getText();
            PaypalEvent paypalEvent = objectMapper.readValue(payload, PaypalEvent.class);

            if (StringUtils.equalsIgnoreCase("subscr_signup", paypalEvent.getType())) {
                String userId = paypalEvent.getUserEmail();
                DateTime today = DateTime.now();
                DateTime mp10 = today.plusDays(40);
                java.sql.Date expiration = new java.sql.Date(mp10.getMillis());
                WedgeUser user = userDao.readUserById(userId);
                if (user == null) {
                    user = new WedgeUser();
                    user.setId(userId);
                    String accessCode = StringGenerator.generateRandomMultiParts(null, "key", new int[]{5, 9});
                    user.setAccessCode(accessCode);
                    if (paypalEvent.getAction() == 1) {
                        user.setApiLimit(4000);
                    } else {
                        user.setApiLimit(1000);
                    }
                    user.setExpirationDate(expiration);
                    user.setPaymentId(paypalEvent.getPayerId());
                    userDao.createUser(user);
                } else {
                    log.warn("User {} is already exist. He may want to take advantage of free trial again.", userId);
                    if (paypalEvent.getAction() == 1) {
                        user.setApiLimit(4000);
                    } else {
                        user.setApiLimit(1000);
                    }
                    user.setExpirationDate(expiration);
                    user.setPaymentId(paypalEvent.getPayerId());
                    userDao.updateUser(user);
                }

                String template = resourceLoader.loadTextFileFromClasspath(emailTemplate);
                if (StringUtils.isBlank(template)) {
                    template = DEFAULT_TEMPLATE;
                }
                template = StringUtils.replace(template, "{{userEmail}}", userId);
                template = StringUtils.replace(template, "{{accessCode}}", user.getAccessCode());
                template = StringUtils.replace(template, "{{apiLimit}}", String.valueOf(user.getApiLimit()));

                EmailRecord email = new EmailRecord();
                String emailId = StringGenerator.generateWedgeCode("EMAIL");
                email.setSubject(emailSubject);
                email.setContent(template);
                email.setFromAddress(fromAddress);
                email.setToAddress(userId);
                email.setId(emailId);
                email.setDomain(emailDomain);
                // Send email notification
                try (CloseableHttpClient client = httpClientFactory.createDefaultHttpClient()) {
                    HttpPost postReq = new HttpPost(emailEndPoint);
                    String json = objectMapper.writeValueAsString(email);
                    StringEntity stringEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
                    postReq.setEntity(stringEntity);
                    HttpResponse response = client.execute(postReq);

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode != 200) {
                        String jsonRes = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());
                        WSErrorResponse errorResponse = objectMapper.readValue(jsonRes, WSErrorResponse.class);
                        log.error("Failed to send order confirmation email {} to {}. Status code {}. {}", emailId, userId, statusCode, errorResponse.getMessage());
                    } else {
                        log.info("Order confirmation email has been sent to {}", userId);
                    }
                } catch (IOException ioe) {
                    log.error("Failed to send order confirmation email {} to {}.", emailId, userId, ioe);
                }
            } else if (StringUtils.equalsIgnoreCase("recurring_payment", paypalEvent.getType()) ||
                    StringUtils.equalsIgnoreCase("subscr_payment", paypalEvent.getType())) {
                String payerId = paypalEvent.getPayerId();
                WedgeUser user = userDao.readUserByPaymentId(payerId);
                if (user != null) {
                    Date expiration = user.getExpirationDate();
                    DateTime jdExp = new DateTime(expiration);
                    DateTime newExp = jdExp.plusDays(31);
                    user.setExpirationDate(new java.sql.Date(newExp.getMillis()));
                    userDao.updateUser(user);
                    log.info("Update user {} expiration date. Payment id {}", user.getId(), paypalEvent.getTnxId());
                } else {
                    log.warn("Couldn't find user under payer id {}. Process terminated for payment {}.", payerId, paypalEvent);
                }
            } else {
                log.warn("Receive unsupported Paypal event {}", paypalEvent);
            }
        } catch (JMSException e) {
            log.error("Failed to load text message for Paypal event. Process terminated.", e);
        } catch (IOException e) {
            log.error("Failed to parse json text into PaypalEvent. Process terminated.", e);
        } catch (Exception e) {
            log.error("Failed to process JMS message with unhandled error.", e);
        }
    }

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserDao userDao;

    @Autowired
    private HttpClientFactory httpClientFactory;

    @Autowired
    private FileResourceLoader resourceLoader;

    @Value("${email.template}")
    private String emailTemplate;

    @Value("${email.from}")
    private String fromAddress;

    @Value("${email.domain}")
    private String emailDomain;

    @Value("${email.subject}")
    private String emailSubject;

    @Value("${entry.point.mail-service}")
    private String emailEndPoint;

}
