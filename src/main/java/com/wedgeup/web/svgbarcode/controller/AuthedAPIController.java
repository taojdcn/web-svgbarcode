package com.wedgeup.web.svgbarcode.controller;

import com.wedgeup.web.svgbarcode.exception.BadRequestException;
import com.wedgeup.web.svgbarcode.handler.BarcodeGenerator;
import com.wedgeup.web.svgbarcode.module.Barcode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by taodong on 11/11/16.
 */

@RestController
@RequestMapping("/secure/v1/barcode")
public class AuthedAPIController {
    private static Logger log = LoggerFactory.getLogger(AuthedAPIController.class);

    @RequestMapping(method= RequestMethod.POST, headers="Accept=application/json")
    public ResponseEntity<String> createBarcode(@Valid @RequestBody Barcode barcode) {
        try {
            String code = barcodeGenerator.generateBarcode(barcode);
            // TODO: update API usage
            return new ResponseEntity(code, HttpStatus.OK);
        } catch (Exception e) {
            log.info("Failed to generate barcode for {}.", barcode, e);
            throw new BadRequestException(e.getMessage());
        }
    }

    @Autowired
    private BarcodeGenerator barcodeGenerator;
}
