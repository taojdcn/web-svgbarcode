package com.wedgeup.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

import javax.jms.ConnectionFactory;
import java.util.Arrays;

@SpringBootApplication
@EnableCaching
@EnableJms
public class SvgbarcodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SvgbarcodeApplication.class, args);
	}

	// JMS configuration
	@Bean
	public JmsListenerContainerFactory<?> jmsListenerFactory(ConnectionFactory connectionFactory,
															 DefaultJmsListenerContainerFactoryConfigurer configurer) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConcurrency("2-10");
		configurer.configure(factory, connectionFactory);
		return factory;
	}

	@Bean
	public JmsTemplate jmsTemplate() {
		JmsTemplate jmsTemplate = new JmsTemplate(jmsFactory());
		jmsTemplate.setDestinationResolver(new DynamicDestinationResolver());
		return jmsTemplate;
	}

	@Bean
	public PooledConnectionFactory jmsFactory() {
		return new PooledConnectionFactory(connectionFactory());
	}

	// ActiveMQ
	@Bean
	public ActiveMQConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory factory =  new ActiveMQConnectionFactory(jmsBrokerUrl);
		return factory;
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	@Bean
	public CacheManager cacheManager() {
		SimpleCacheManager cacheManager = new SimpleCacheManager();
		cacheManager.setCaches(Arrays.asList(
				new ConcurrentMapCache("templates"),
				new ConcurrentMapCache("wedge-users")
		));
		return cacheManager;
	}

	@Value("${spring.activemq.broker-url}")
	private String jmsBrokerUrl;
}
